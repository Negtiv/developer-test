<?php

namespace App\Services;

use Illuminate\Support\Str;

class PhoneNumberService
{
    public function filterEndingIn(string $value, array $numbers): array
    {
        return array_filter($numbers, fn ($number) => Str::endsWith($number, $value));
    }
}
