<?php

namespace App\Collections;

use Illuminate\Support\Collection;

class NumbersCollection extends Collection
{
    public function greaterThanOrEqualTo(int $value): self
    {
        return $this->filter(fn (int $number): bool => $number >= $value);
    }
}
