<?php

namespace App\Collections;

use App\DataObjects\BrandData;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class BrandsCollection extends Collection
{
    public static function make($items = []): self
    {
        return (new static($items))
            ->mapWithKeys(fn ($data, $brand) => [$brand => new BrandData($brand, $data['models'])]);
    }
}
