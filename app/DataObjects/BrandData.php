<?php

namespace App\DataObjects;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class BrandData
{
    public string $brand;
    public Collection $models;

    public function __construct(
        string $brand,
        array $models
    )
    {
        $this->models = collect($models);
    }

    public function hasModelsContaining(string $value): bool
    {
        return $this->models
            ->filter(fn (string $model) => Str::contains($model, $value))
            ->isNotEmpty();
    }
}
