<?php

namespace App\DataObjects;

class PeopleData
{
    protected string $firstName;
    protected string $lastname;
    protected string $gender;

    public function __construct(array $person)
    {
        $this->firstName = $person['first-name'];
        $this->lastname = $person['last-name'];
        $this->gender = $person['gender'];
    }

    public function isMale(): bool
    {
        return $this->gender === 'male';
    }

    public function fullName(): string
    {
        return sprintf('%s %s', $this->firstName, $this->lastname);
    }
}
