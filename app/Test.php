<?php namespace App;

use App\Collections\BrandsCollection;
use App\Collections\NumbersCollection;
use App\DataObjects\PeopleData;
use App\Repositories\Contracts\TestDataRepositoryInterface;
use App\Services\PhoneNumberService;

class Test implements TestInterface
{
    protected TestDataRepositoryInterface $testDataRepository;

    public function __construct(TestDataRepositoryInterface $testDataRepository)
    {
        $this->testDataRepository = $testDataRepository;
    }

    public function returnFullNameOfPeopleWithGender(string $gender): array
    {
        return collect($this->testDataRepository->getPeople())
            ->mapInto(PeopleData::class)
            ->filter->isMale()
            ->map->fullName()
            ->toArray();
    }

    public function returnAllBrandNamesWithModelContaining(string $string): array
    {
        return BrandsCollection::make($this->testDataRepository->getCars()['brands'])
            ->filter->hasModelsContaining($string)
            ->keys()
            ->toArray();
    }

    public function returnSumAllNumbersGreaterOrEqualThan(int $number): int
    {
        return NumbersCollection::make($this->testDataRepository->getNumbers())
            ->greaterThanOrEqualTo($number)
            ->sum();
    }

    public function returnPhoneNumberEndingIn(string $string): array
    {
        return app(PhoneNumberService::class)
            ->filterEndingIn($string, $this->testDataRepository->getPhoneNumbers());
    }
}
